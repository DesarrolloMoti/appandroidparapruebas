package com.esquinca.motiprintslogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton

class LoginActivity : AppCompatActivity() {

    lateinit var callbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        FacebookSdk.sdkInitialize(applicationContext);

        callbackManager = CallbackManager.Factory.create()

        val loginButtonFacebook = findViewById<LoginButton>(R.id.login_button)
        loginButtonFacebook.setPermissions(listOf("public_profile", "email", "user_photos" ))
        //loginButtonFacebook.setPermissions(listOf("public_profile", "email"))

        loginButtonFacebook.registerCallback(callbackManager, object :
            FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                val request = GraphRequest.newMeRequest(loginResult!!.accessToken) { jsonObject, response ->
                    Log.e("LOG_TAG", "onCompleted jsonObject: $jsonObject")
                    Log.e("loginResult", "onCompleted jsonObject: $loginResult")
                    //loginFB(jsonObject!!.getString("id").toString(), jsonObject.getString("name").toString(), jsonObject.getString("email").toString())
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,link,cover,email")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                Toast.makeText(this@LoginActivity, "Login Cancelled", Toast.LENGTH_LONG).show()
            }

            override fun onError(exception: FacebookException) {
                Log.e("TAG", exception.message.toString())
                Toast.makeText(this@LoginActivity, exception.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}